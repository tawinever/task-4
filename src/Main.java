import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Random;
import java.util.*;

public class Main {
    static String MSG_MATRIX_SIZE = "Enter matrix size please(should be more than 0):";
    static String MSG_MATRIX_VALUE_THRESHOLD = "Enter maximum absolute value(should be non-negative integer):";
    static String MSG_CHOOSE_OPERATION = "Choose 0 to sort by row, 1 to sort by column:";
    static String MSG_CHOOSE_INDEX = "Choose index of sorting row/column(start from 0)";
    static Random random = null;

    public enum OperationType {
        ROW,
        COLUMN,
    }

    public enum State {
        WAITING_FIRST_POSITIVE,
        WAITING_SECOND_POSITIVE,
        PASSED_SECOND_POSITIVE
    }

    public static void main(String[] args) {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int matrixSize = getValidIntegerInput(bufferedReader, 1, Integer.MAX_VALUE, MSG_MATRIX_SIZE);
        int maxAbsoluteValue = getValidIntegerInput(bufferedReader, 0,
                Integer.MAX_VALUE, MSG_MATRIX_VALUE_THRESHOLD);

        System.out.println("\nGenerated matrix:\n");
        int[][] matrix = seedMatrix(matrixSize, maxAbsoluteValue);
        print(matrix);


        OperationType operation = OperationType.values()[getValidIntegerInput(bufferedReader,0, 1,
                MSG_CHOOSE_OPERATION)];

        int sortingIndex = getValidIntegerInput(bufferedReader, 0, matrixSize - 1, MSG_CHOOSE_INDEX);

        try {
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("\nSorted matrix:\n");
        sortMatrix(matrix, operation, sortingIndex);
        print(matrix);

        System.out.println("\nSum between two positive number:\n");
        printSumBetweenFirstAndSecondPositive(matrix);

        System.out.println("\nNew matrix:\n");
        Vector<Vector<Integer>> newMatrix = removeMaxElement(matrix);
        print(newMatrix);

    }

    public static int getValidIntegerInput(BufferedReader bufferedReader, int minValue, int maxValue, String message) {
        String number = null;
        int validNumber = -1;
        while (number == null) {
            System.out.println(message);
            try {
                number = bufferedReader.readLine();
                validNumber = Integer.parseInt(number.trim());
                if(validNumber < minValue || validNumber > maxValue)
                    throw new IOException();
            } catch (IOException e) {
                System.out.println("Invalid data");
                number = null;
            }
        }
        return validNumber;
    }

    public static int[][] seedMatrix(int matrixSize, int threshold) {
        int[][] matrix = new int[matrixSize][matrixSize];
        for (int i = 0; i < matrixSize; i++) {
            for (int j = 0; j < matrixSize; j++) {
                matrix[i][j] = getRandomNumber(threshold * (-1), threshold);
            }
        }
        return matrix;
    }

    public static int getRandomNumber(int minValue, int maxValue) {
        if (random == null) {
            random = new Random();
        }
        return random.nextInt(maxValue - minValue + 1) + minValue;
    }

    public static void print(int[][] matrix) {
        for (int[] row : matrix) {
            for (int cell : row) {
                System.out.print(cell + " ");
            }
            System.out.println();
        }
    }

    public static void sortMatrix(int[][] matrix, OperationType operation, int sortingIndex) {
        Pair[] permutation = new Pair[matrix[0].length];
        for (int i = 0; i < matrix[0].length; ++i) {
            if(operation == OperationType.ROW) {
                permutation[i] = new Pair(matrix[sortingIndex][i], i);
            } else {
                permutation[i] = new Pair(matrix[i][sortingIndex], i);
            }
        }

        Arrays.sort(permutation, Comparator.comparingInt(a -> a.first));

        int[] tmp = new int[matrix[0].length];

        for (int i = 0; i < matrix[0].length; ++i) {
            for (int j = 0; j < matrix[0].length; ++j) {
                if (operation == OperationType.ROW) {
                    tmp[j] = matrix[i][permutation[j].second];
                } else {
                    tmp[j] = matrix[permutation[j].second][i];
                }
            }

            for (int j = 0; j < matrix[0].length; ++j) {
                if (operation == OperationType.ROW) {
                    matrix[i][j] = tmp[j];
                } else {
                    matrix[j][i] = tmp[j];
                }
            }
        }
    }

    public static void printSumBetweenFirstAndSecondPositive(int[][] matrix) {
        for (int i = 0; i < matrix[0].length; ++i) {
            State state = State.WAITING_FIRST_POSITIVE;
            int firstPositive = 0;
            int secondPositive = 0;
            int sum = 0;
            for (int x : matrix[i]) {
                if(state == State.WAITING_FIRST_POSITIVE) {
                    if(x > 0) {
                        firstPositive = x;
                        state = State.WAITING_SECOND_POSITIVE;
                    }
                } else if (state == State.WAITING_SECOND_POSITIVE) {
                    if(x > 0) {
                        secondPositive = x;
                        state = State.PASSED_SECOND_POSITIVE;
                    } else {
                        sum += x;
                    }
                } else {
                    break;
                }
            }
            if(state == State.PASSED_SECOND_POSITIVE) {
                System.out.println(String.format("%d. Elements: %d, %d; Sum: %d;",
                        i + 1, firstPositive, secondPositive, sum));
            } else if (state == State.WAITING_SECOND_POSITIVE) {
                System.out.println(String.format("%d. Element: %d, ; Sum: 0;", i + 1, firstPositive));
            } else {
                System.out.println(String.format("%d. No Positive Elements; Sum: 0;", i + 1));
            }
        }
    }

    public static Vector<Vector<Integer>> removeMaxElement(int[][] matrix) {
        int maxValue = Integer.MIN_VALUE;

        for (int[] row : matrix) {
            for (int x : row) {
                maxValue = Math.max(x, maxValue);
            }
        }

        Set<Integer> rows = new HashSet<>();
        Set<Integer> cols = new HashSet<>();

        for (int i = 0; i < matrix[0].length; ++i) {
            for (int j = 0; j < matrix[0].length; ++j) {
                if (matrix[i][j] == maxValue) {
                    rows.add(i);
                    cols.add(j);
                }
            }
        }


        Vector<Vector<Integer>> newMatrix = new Vector<>(0);

        for (int i = 0; i < matrix[0].length; ++i) {
            if(rows.contains(i)) {
                continue;
            }
            newMatrix.add(new Vector<>(0));
            for (int j = 0; j < matrix[0].length; ++j) {
                if (cols.contains(j)) {
                    continue;
                }
                newMatrix.lastElement().add(matrix[i][j]);
            }
        }
        return newMatrix;
    }

    public static void print(Vector<Vector<Integer>> matrix) {
        for (Vector<Integer> row : matrix) {
            for (int cell : row) {
                System.out.print(cell + " ");
            }
            System.out.println();
        }
    }
}
